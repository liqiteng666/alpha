package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.LoginVo;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: liqiteng
 * @Date: 2023/06/27/16:52
 * @Description:
 */
public interface LoginService {

    String login(LoginVo loginVo);
}
