package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: liqiteng
 * @Date: 2023/06/27/15:36
 * @Description:
 */
@Data
@TableName("user")
public class User {

    @TableId
    private Long id;
    @TableField("user_name")
    private String userName;
    @TableField("password")
    private String password;
}
